# PBbackup Scripts - README #
---

### Overview ###

The **PBbackup** scripts (Bash) can be used to make backups (dumps) of MySQL databases with or without compression and backups of a specified directory (webroot). The backups can also be copied to a NAS share.

### Screenshots ###

![PBbackup - Database backup](development/readme/pbbackup1.png "PBbackup - Database backup")

![PBbackup - Directory backup](development/readme/pbbackup2.png "PBbackup - Directory backup")

### Setup ###

* Copy the directory **pbbackup** to your webhost.
* Be sure that the directory **pbbackup** is not inside your webroot!
* Open the three scripts in your favorite editor and customize the configuration sections.
* Make the three scripts executable (chmod +x).
* The script **db_backup.sh** is used to backup MySQL databases.
* The script **dir_backup.sh** is used to backup a specified folder.
* The script **del_backup.sh** is used to delete al local backups (not backups on the NAS share!).
* Test the scripts from the command line.
* Create some cronjobs to run the backups automatically.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBbackup** scripts are licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
