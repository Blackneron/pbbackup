#!/bin/bash


########################################################################
#              C O N F I G U R A T I O N   -   B E G I N               #
########################################################################

# Specify the destination directory.
destination_dir="/john/pbbackup/data/local"


########################################################################
#               D E L E T E   L O C A L   B A C K U P S                #
########################################################################

# Count the SQL backup files.
count=$(ls -1 $destination_dir/*.sql 2>/dev/null | wc -l)

# Check if SQL backup files exist.
if [ $count != 0 ]; then

  # Delete SQL backup files.
  rm $destination_dir/*.sql

fi

# Count the 7z backup files.
count=$(ls -1 $destination_dir/*.7z 2>/dev/null | wc -l)

# Check if 7z backup files exist.
if [ $count != 0 ]; then

  # Delete 7z backup files.
  rm $destination_dir/*.7z

fi
