#!/bin/bash


########################################################################
#              C O N F I G U R A T I O N   -   B E G I N               #
########################################################################

# Specify the directory to backup.
source_dir="/var/www/cms"

# Specify the destination directory.
destination_dir="/john/pbbackup/data/local"

# Specify the name for the backup.
backup_name="cms"

# Specify the date format for the backup name.
backup_date=$(date +"%Y-%m-%d_%H:%M:%S")

# Specify the NAS username.
nas_username="john"

# Specify the NAS password.
nas_password="top_secret"

# Specify IP address of the NAS (host).
nas_ip="192.168.0.111"

# Specify the NAS share which has to be mounted.
nas_share="Data/Application1"

# Specify the mount point (without the share name).
nas_mount_point="/john/pbbackup/data/external"


########################################################################
#     B A C K U P   T H E   S P E C I F I E D   D I R E C T O R Y      #
########################################################################

# Compress the specified directory.
7z a -t7z $destination_dir/$backup_name-$backup_date.7z $source_dir -ms -mmt


########################################################################
#           D E L E T E   O L D   L O C A L   B A C K U P S            #
########################################################################

# Delete archive files older than 30 days.
find $destination_dir/* -name *.7z -mtime +30 -exec rm {} \;


########################################################################
#                  C R E A T E   M O U N T P O I N T                   #
########################################################################

# Check if the mountpoint does not exist.
if [ ! -d "$nas_mount_point" ]; then

  # Create the mountpoint.
  mkdir -p "$nas_mount_point"

fi


########################################################################
#                    M O U N T   N A S   S H A R E                     #
########################################################################

# Check if the NAS share is not already mounted.
if [ $(mountpoint "$nas_mount_point" | grep "not" | wc -l) == 1 ]; then

  # Mount the NAS share.
  sudo mount -t cifs -o username="$nas_username",password="$nas_password",uid=1000,gid=1000,iocharset="utf8",rw,dir_mode=0777,file_mode=0666 //"$nas_ip"/"$nas_share" "$nas_mount_point"

fi


########################################################################
#           C O P Y   B A C K U P   T O   N A S   S H A R E            #
########################################################################

# Check if the NAS share is mounted.
if [ ! $(mountpoint "$nas_mount_point" | grep "not" | wc -l) == 1 ]; then

  # Check if the new backup archive does exist.
  if [ -f "$destination_dir/$backup_name-$backup_date.7z" ]; then

    # Copy the plain text database backup to the NAS.
    cp $destination_dir/$backup_name-$backup_date.7z $nas_mount_point/$backup_name-$backup_date.7z

  fi

fi


########################################################################
#                    D I S P L A Y   B A C K U P S                     #
########################################################################

# Show the existing local backup files.
clear
echo Existing Local Backups:
echo -----------------------
find $destination_dir/* -name $backup_name-*.7z
echo

# Show the existing remote backup files.
echo Existing Remote Backups:
echo ------------------------
find $nas_mount_point/* -name $backup_name-*.7z
echo


########################################################################
#                  U N M O U N T   N A S   S H A R E                   #
########################################################################

# Check if the NAS share is mounted.
if [ ! $(mountpoint "$nas_mount_point" | grep "not" | wc -l) == 1 ]; then

  # Unmount the actual share.
  sudo umount $nas_mount_point

fi
