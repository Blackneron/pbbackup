#!/bin/bash


########################################################################
#              C O N F I G U R A T I O N   -   B E G I N               #
########################################################################

# Specify the MySQL database user.
db_user="john"

# Specify the MySQL database password.
db_password="top_secret"

# Specify the MySQL database host.
db_host="localhost"

# Specify the MySQL database names (db1 db2 db3).
db_names=(lessons)

# Specify the local backup path.
db_backup_path="/john/pbbackup/data/local"

# Specify the date format for the backup name.
db_date=$(date +"%Y-%m-%d_%H:%M:%S")

# Specify the NAS username.
nas_username="john"

# Specify the NAS password.
nas_password="top_secret"

# Specify IP address of the NAS (host).
nas_ip="192.168.0.111"

# Specify the NAS share which has to be mounted.
nas_share="Data/Application1"

# Specify the mount point (without the share name).
nas_mount_point="/john/pbbackup/data/external"

# Specify if the backup should be compressed.
compress_backup=0

# Specify the default file permissions.
umask 177


########################################################################
#             B A C K U P   ( D A T A B A S E   D U M P )              #
########################################################################

# Loop through all the specified databases.
for db_name in "${db_names[@]}"
do

  # Dump the database into an SQL file.
  sudo mysqldump --user=$db_user --password=$db_password --host=$db_host $db_name > $db_backup_path/db-$db_name-$db_date.sql

done # Loop through all the specified databases.


########################################################################
#           C O M P R E S S   D A T A B A S E   B A C K U P            #
########################################################################

# Check if the backup should be compressed.
if [ $compress_backup == 1 ]; then

  # Compress the backup file.
  7z a $db_backup_path/db-$db_name-$db_date.7z $db_backup_path/db-$db_name-$db_date.sql

  # Remove the original sql file.
  rm -I $db_backup_path/db-$db_name-$db_date.sql

fi


########################################################################
#           D E L E T E   O L D   L O C A L   B A C K U P S            #
########################################################################

# Delete files older than 30 days.
find $db_backup_path/* -name *.sql -o -name *.7z -mtime +30 -exec rm {} \;


########################################################################
#                  C R E A T E   M O U N T P O I N T                   #
########################################################################

# Check if the mountpoint does not exist.
if [ ! -d "$nas_mount_point" ]; then

  # Create the mountpoint.
  mkdir -p "$nas_mount_point"

fi


########################################################################
#                    M O U N T   N A S   S H A R E                     #
########################################################################

# Check if the NAS share is not already mounted.
if [ $(mountpoint "$nas_mount_point" | grep "not" | wc -l) == 1 ]; then

  # Mount the NAS share.
  sudo mount -t cifs -o username="$nas_username",password="$nas_password",uid=1000,gid=1000,iocharset="utf8",rw,dir_mode=0777,file_mode=0666 //"$nas_ip"/"$nas_share" "$nas_mount_point"

fi


########################################################################
#           C O P Y   B A C K U P   T O   N A S   S H A R E            #
########################################################################

# Check if the NAS share is mounted.
if [ ! $(mountpoint "$nas_mount_point" | grep "not" | wc -l) == 1 ]; then

  # Check if the new plain text backup does exist.
  if [ -f "$db_backup_path/db-$db_name-$db_date.sql" ]; then

    # Copy the plain text database backup to the NAS.
    cp $db_backup_path/db-$db_name-$db_date.sql $nas_mount_point/db-$db_name-$db_date.sql

  fi

  # Check if the compressed text backup does exist.
  if [ -f "$db_backup_path/db-$db_name-$db_date.7z" ]; then

    # Copy the compressed database backup to the NAS.
    cp $db_backup_path/db-$db_name-$db_date.7z $nas_mount_point/db-$db_name-$db_date.7z

  fi

fi


########################################################################
#                    D I S P L A Y   B A C K U P S                     #
########################################################################

# Show the existing local backup files.
clear
echo Existing Local Backups:
echo -----------------------
find $db_backup_path/* -name db-*.sql -o -name db-*.7z
echo

# Show the existing remote backup files.
echo Existing Remote Backups:
echo ------------------------
find $nas_mount_point/* -name db-*.sql -o -name db-*.7z
echo


########################################################################
#                  U N M O U N T   N A S   S H A R E                   #
########################################################################

# Check if the NAS share is mounted.
if [ ! $(mountpoint "$nas_mount_point" | grep "not" | wc -l) == 1 ]; then

  # Unmount the actual share.
  sudo umount $nas_mount_point

fi
